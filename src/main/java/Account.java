public class Account {
    String username;
    String password;
    int cups;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public int getCups() {
        return cups;
    }

    public void increaseCups() {
        cups++;
    }

    public void reduceCups() {
        cups--;
        if (cups < 0) {
            cups = 0;
        }
    }
}
