import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.net.Socket;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class ClientHandler extends Thread {
    private Socket socket;
    private Server server;
    private String clientName;
    private PrintStream printer;
    private Scanner scanner;
    private String hero;
    private int retry = 0;
    private Account account;
    private Gson gson;

    private GameState gameState;

    private Class<?> gameMode;
    private String gameModeName;


    ClientHandler(Server server, Socket socket) {
        this.server = server;
        this.socket = socket;
        gson = new Gson();
        gameState = new GameState(server.getCards());
        clientName = socket.getRemoteSocketAddress().toString();
        gameState.initialize(server.getCards());
        try {
            scanner = new Scanner(socket.getInputStream());
            printer = new PrintStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (isStillAlive()) {
            String message;
            try {
                message = scanner.nextLine();
                retry = 0;
            } catch (NoSuchElementException e) {
                server.notifyDisconnected(this);
                if (retry == 30) {
                    server.notifyAbandon(this);
                    break;
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
                retry++;

                continue;
            }

            switch (message) {
                case "register": {
                    System.out.println("register requested");
                    String player;
                    player = scanner.nextLine();
                    boolean success = server.registerPlayer(player);
                    if (!success) {
                        send("fail");
                        System.out.println("register failed");
                        server.removeClient(this);
                    } else {
                        send("success");
                        send(player);
                        System.out.println("register success");
                        account = Utils.getAccountFromJson(player);
                    }
//                    synchronized (Server.lock) {
//                        Server.lock.notify();
//                    }
//                    server.initializeOther(this, scanner.nextLine());
                    break;
                }
                case "login": {
                    String username = scanner.nextLine();
                    String password = scanner.nextLine();
                    String player = server.loginPlayer(username, password);
                    if (player == null) {
                        send("fail");
//                        server.removeClient(this);
                    } else {
                        send("success");
                        send(player);
                        account = Utils.getAccountFromJson(player);
                    }
                    break;
                }
                case "disconnect":
                    System.out.println("client disconnected");
                    server.removeClient(this);
                    break;
                case "requestMatch":
                    this.hero = scanner.nextLine();
                    String gameType = scanner.nextLine();
                    try {
                        initializeGameType(gameType);
                    } catch (IOException | NoSuchMethodException | ClassNotFoundException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
                        e.printStackTrace();
                    }
                    server.queueForMatchMaking(this);
//                    send("gameState");
//                    send(gson.toJson(gameState));
                    break;
                case "endTurn":
                    System.out.println(clientName + " turn ended");
                    String gameStateJson = scanner.nextLine();
                    gameState = (gson.fromJson(gameStateJson, GameState.class));
                    gameState.update(gameMode);
                    sendGameState();
                    server.endTurn(this);
                    break;
                case "playCard":
                    String cardName = scanner.nextLine();
                    String position = scanner.nextLine();
                    System.out.println(clientName + " played " + cardName);
                    server.playCard(this, cardName, position);
                    break;
                case "attackMinion": {
                    String selectedCard = scanner.nextLine();
                    String targetCard = scanner.nextLine();
                    server.attackMinion(this, selectedCard, targetCard);
                    break;
                }
                case "damageMinion": {
                    String targetCard = scanner.nextLine();
                    String damage = scanner.nextLine();
                    server.damageMinion(this, targetCard, damage);
                    break;
                }
                case "attackHero": {
                    String selectedCard = scanner.nextLine();
                    server.attackHero(this, selectedCard);
                    break;
                }
                case "damageHero": {
                    String damage = scanner.nextLine();
                    server.damageHero(this, damage);
                    break;
                }
                case "matchFinished": {
                    String playerData = scanner.nextLine();
                    try {
                        server.updatePlayerData(this, playerData);
                        server.finishMatch(this);
                        send("matchFinished");
                    } catch (IOException e) {
                        send("saveFailed");
                        e.printStackTrace();
                    }
                    break;
                }
                case "chat": {
                    String chatMessage = scanner.nextLine();
                    server.sendMessage(this, chatMessage);
                    break;
                }
                case "leaderBoard": {
                    System.out.println("leaderBoard");
                    server.getLeaderBoard();
                    break;
                }
                case "requestCards":
                    if (!server.isConnectedToDatabase()) {
                        send("databaseFailed");
                    }
                    System.out.println("cardsRequested");
                    send("cards");
                    send(gson.toJson(server.getCards()));
                    break;
                case "requestHeroes":
                    if (!server.isConnectedToDatabase()) {
                        send("databaseFailed");
                    }
                    System.out.println("heroesRequested");
                    send("heroes");
                    send(gson.toJson(server.getHeroes()));
                    break;
                case "requestGameMode":
                    if (!server.isConnectedToDatabase()) {
                        send("databaseFailed");
                    }
                    send("gameModes");
                    send(String.valueOf(server.getGameModes().size()));
                    for (String gameMode : server.getGameModes()) {
                        send(gameMode);
                    }
                    break;
                case "deleteAccount":
                    System.out.println("deleteAccount");
                    String password = scanner.nextLine();
                    try {
                        deleteAccount(password);
                        send("success");
                    } catch (IOException e) {
                        send("fail");
                    }
                    break;
                case "abandon":
                    server.notifyAbandon(this);
//                    server.finishMatch(this);
                    break;
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
                break;
            }
        }
    }

    private void deleteAccount(String password) throws IOException {
        if (Password.decrypt(account.getPassword()).equals(password)) {
            File file = new File(String.format("src/main/resources/players/%s.json", account.getUsername()));
            Files.delete(file.toPath());
        } else {
            throw new IOException();
        }
    }

    private void initializeGameType(String gameModeName) throws IOException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        this.gameModeName = gameModeName;
        for (File gameModeFile : (new File("src/main/resources/tavern_brawl")).listFiles()) {
            if (gameModeFile.getName().contains(gameModeName)) {
                String address = "file:///C:/Users/xerxes/IdeaProjects/Hearthstone_Server/src/main/resources/tavern_brawl/" + gameModeFile.getName();
                URLClassLoader loader = new URLClassLoader(new URL[]{new URL(address)}, ClientHandler.class.getClassLoader());

                gameMode = Class.forName("Main", true, loader);
                gameState.initialize(gameMode, server.getCards());
                break;
            }
        }
    }

    private boolean isStillAlive() {
        return socket.isConnected();
    }

    public void send(String message) {
        printer.println(message);
    }

    public Account getAccount() {
        return account;
    }

    public Integer getCups() {
        return account.getCups();
    }

    public void endTurn() {
        sendGameState();
        send("endTurn");
    }

    public GameState getGameState() {
        return gameState;
    }

    public void sendGameState() {
        send("gameState");
        send(gson.toJson(gameState));
    }

    public String getGameModeName() {
        return gameModeName;
    }

    public String getClientName() {
        return clientName;
    }

    public String getHero() {
        return hero;
    }
}
