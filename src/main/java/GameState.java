import card.Card;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class GameState {
    // Variables
    private List<Card> cards;
    private Integer playedCardsCount;
    private List<List<Card>> decks;
    private Integer timeSpentOnPlay;
    private Integer otherTurnTimeLimit;
    // Constants
    private Integer manaCap;
    private Integer manaPerTurn;
    private Integer turnTimeLimit;
    private Integer holdCardsLimit;
    private Integer playedCardsLimit;

    public GameState(List<Card> cards) {
        this.cards = cards;

    }

    public void update(Class<?> gameMode) {
        if(gameMode== null){
            return;
        }
        try {
            gameMode.getMethod("update", Object.class).invoke(null, this);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public void initialize(List<Card> cards){
        this.cards = cards;
        playedCardsCount = 0;
        decks = new ArrayList<>();
        timeSpentOnPlay = 0;

        manaCap = 0;
        manaPerTurn = 1;
        turnTimeLimit = 60;
        otherTurnTimeLimit = 60;
        holdCardsLimit = 12;
        playedCardsLimit = 7;
    }

    public void initialize(Class<?> gameMode, List<Card> cards) {
        initialize(cards);

        try {
            gameMode.getMethod("initialize", Object.class).invoke(null, this);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public int getPlayedCardsCount() {
        return playedCardsCount;
    }

    public void setPlayedCardsCount(int playedCardsCount) {
        this.playedCardsCount = playedCardsCount;
    }

    public List<List<Card>> getDecks() {
        return decks;
    }

    public void setDecks(List<List<Card>> decks) {
        this.decks = decks;
    }

    public int getTimeSpentOnPlay() {
        return timeSpentOnPlay;
    }

    public void setTimeSpentOnPlay(int timeSpentOnPlay) {
        this.timeSpentOnPlay = timeSpentOnPlay;
    }

    public int getManaCap() {
        return manaCap;
    }

    public void setManaCap(int manaCap) {
        this.manaCap = manaCap;
    }

    public int getManaPerTurn() {
        return manaPerTurn;
    }

    public void setManaPerTurn(int manaPerTurn) {
        this.manaPerTurn = manaPerTurn;
    }

    public int getTurnTimeLimit() {
        return turnTimeLimit;
    }

    public void setTurnTimeLimit(int turnTimeLimit) {
        this.turnTimeLimit = turnTimeLimit;
    }

    public int getHoldCardsLimit() {
        return holdCardsLimit;
    }

    public void setHoldCardsLimit(int holdCardsLimit) {
        this.holdCardsLimit = holdCardsLimit;
    }

    public int getPlayedCardsLimit() {
        return playedCardsLimit;
    }

    public void setPlayedCardsLimit(int playedCardsLimit) {
        this.playedCardsLimit = playedCardsLimit;
    }

    public void setOtherTurnTimeLimit(int otherTurnTimeLimit) {
        this.otherTurnTimeLimit = otherTurnTimeLimit;
    }
}
