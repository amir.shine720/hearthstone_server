import card.Card;
import card.Hero;
import card.HeroPower;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import database.MySqlMain;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.util.*;

public class Server extends Thread {
    public static final Integer lock = 0;
    MySqlMain database;
    private int serverPort = 8001;
    private List<ClientHandler> clients;
    private ServerSocket serverSocket;
    private List<List<ClientHandler>> playingPlayers;
    private List<Card> cards;
    private List<Hero.HeroClass> heroes;
    private boolean connectedToDatabase;
    private List<String> gameModes;


    public Server() throws IOException {
        try {
            connectToDatabase();
            connectedToDatabase = true;
        } catch (SQLException throwables) {
            connectedToDatabase = false;
            throwables.printStackTrace();
        }
        countGameModes();
        serverSocket = new ServerSocket(serverPort);
        playingPlayers = new ArrayList<>();
        clients = new ArrayList<>();
        System.out.println("Server Started at: " + serverPort);
    }

    private static String getAsString(List<?> list) {
        StringBuilder result = new StringBuilder();
        for (Object object : list) {
            result.append(object.toString());
            result.append(",");
        }
        result.deleteCharAt(result.length() - 1);
        return String.valueOf(result);
    }

    private void countGameModes() {
        gameModes = new ArrayList<>();
        for (File gameMode : (new File("src/main/resources/tavern_brawl")).listFiles()) {
            int extensionPoint = gameMode.getName().lastIndexOf(".");
            String name = gameMode.getName().substring(0, extensionPoint);
            gameModes.add(name);
        }
    }

    private void connectToDatabase() throws SQLException {
        database = new MySqlMain();
        List<Map<String, Object>> cardsResultMap = database.getReader().getDataSet("SELECT * FROM cards",
                Arrays.asList("id", "name", "description", "manaCost", "hero", "type", "rarity", "attack", "health", "heroDefaultId"));
        List<Map<String, Object>> heroPowersResultMap = database.getReader().getDataSet("SELECT * FROM heroPowers",
                Arrays.asList("id", "name", "description", "manaCost", "needTarget"));
        List<Map<String, Object>> heroesResultMap = database.getReader().getDataSet("SELECT * FROM heroes",
                Arrays.asList("id", "name", "specialPower", "heroPower"));

        Map<Integer, HeroPower> heroPowers = new HashMap<>();

        for (Map<String, Object> heroPowersMap : heroPowersResultMap) {
            heroPowers.put((Integer) heroPowersMap.get("id"), new HeroPower((String) heroPowersMap.get("name"),
                    (String) heroPowersMap.get("description"), (Integer) heroPowersMap.get("manaCost"),
                    (Boolean) heroPowersMap.get("needTarget")));
        }


        Map<Integer, Hero.HeroClass> heroes = new HashMap<>();

        for (Map<String, Object> heroesMap : heroesResultMap) {
            heroes.put(((Integer) heroesMap.get("id")), new Hero.HeroClass(((String) heroesMap.get("name")), new ArrayList<>(), ((String) heroesMap.get("specialPower")),
                    heroPowers.get(((Integer) heroesMap.get("heroPower")))));
        }
//        Hero.setHeroes(new ArrayList<>(heroes.values()));

        Map<Integer, Card> cards = new HashMap<>();

        for (Map<String, Object> cardsMap : cardsResultMap) {
            cards.put(((Integer) cardsMap.get("id")), new Card(((String) cardsMap.get("name")), ((String) cardsMap.get("description")),
                    ((Integer) cardsMap.get("manaCost")),
                    Hero.getByName(heroes.get(((Integer) cardsMap.get("hero"))).getName()),
                    ((String) cardsMap.get("type")), ((String) cardsMap.get("rarity")),
                    ((Integer) cardsMap.get("attack")), ((Integer) cardsMap.get("health"))));
            Object defaultId = cardsMap.get("heroDefaultId");
            if (defaultId != null) {
                int index = ((Integer) cardsMap.get("heroDefaultId"));
                heroes.get(index).addDefaultCard((String) cardsMap.get("name"));
//                Hero.getByName(heroes.get(index).getName()).getDefaultCards().add(((String) cardsMap.get("name")));
            }
        }

        this.cards = new ArrayList<>(cards.values());
        this.heroes = new ArrayList<>(heroes.values());

    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            try {
                Socket socket = serverSocket.accept();

                ClientHandler clientHandler = new ClientHandler(this, socket);
                clients.add(clientHandler);
                System.out.println("Client at: " + socket.getRemoteSocketAddress().toString() + " is connected.");
                clientHandler.start();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void queueForMatchMaking(ClientHandler client) {
        boolean matched = false;
        for (List<ClientHandler> playerDuo : playingPlayers) {
            if (playerDuo.get(1) == null && playerDuo.get(0).getGameModeName().equals(client.getGameModeName())) {
                playerDuo.set(1, client);
                initializeOther(playerDuo.get(0), playerDuo.get(0).getHero());
                initializeOther(playerDuo.get(1), playerDuo.get(1).getHero());

                playerDuo.get(0).getGameState().setOtherTurnTimeLimit(playerDuo.get(1).getGameState().getTurnTimeLimit());
                playerDuo.get(1).getGameState().setOtherTurnTimeLimit(playerDuo.get(0).getGameState().getTurnTimeLimit());

                playerDuo.get(0).sendGameState();
                playerDuo.get(1).sendGameState();

                client.send("matchedWith");
                client.send(playerDuo.get(0).getClientName());
                playerDuo.get(0).send("matchedWith");
                playerDuo.get(0).send(playerDuo.get(1).getClientName());

                Random random = new Random(System.nanoTime());
                playerDuo.get(random.nextInt(2)).send("turn");
                System.out.println("turn sent");
                matched = true;
                System.out.println("Clients matched!!");
                break;
            }
        }
        if (!matched) {
            playingPlayers.add(new ArrayList<>(Arrays.asList(client, null)));
            System.out.println("Client is in queue for match: ");
            client.send("wait");
        }
    }

    public void initializeOther(ClientHandler clientHandler, String hero) {
        ClientHandler otherPlayer = getOtherPlayer(clientHandler);
        if (otherPlayer != null) {
            otherPlayer.send("initialize");
            otherPlayer.send(hero);

        }
    }

    public void endTurn(ClientHandler clientHandler) {
        ClientHandler otherPlayer = getOtherPlayer(clientHandler);
        if (otherPlayer != null) {
            otherPlayer.getGameState().setOtherTurnTimeLimit(clientHandler.getGameState().getTurnTimeLimit());
            otherPlayer.endTurn();
        }
    }

    public void playCard(ClientHandler clientHandler, String cardName, String position) {
        ClientHandler otherPlayer = getOtherPlayer(clientHandler);
        if (otherPlayer != null) {
            otherPlayer.send("playCard");
            otherPlayer.send(cardName);
            otherPlayer.send(position);
        }
    }

    public void attackMinion(ClientHandler clientHandler, String selectedCard, String targetCard) {
        ClientHandler otherPlayer = getOtherPlayer(clientHandler);
        if (otherPlayer != null) {
            otherPlayer.send("attackMinion");
            otherPlayer.send(selectedCard);
            otherPlayer.send(targetCard);
        }
    }

    public void damageMinion(ClientHandler clientHandler, String targetCard, String damage) {
        ClientHandler otherPlayer = getOtherPlayer(clientHandler);
        if (otherPlayer != null) {
            otherPlayer.send("damageMinion");
            otherPlayer.send(targetCard);
            otherPlayer.send(damage);
        }
    }

    private ClientHandler getOtherPlayer(ClientHandler clientHandler) {
        for (List<ClientHandler> playerDuo : playingPlayers) {
            if (playerDuo.contains(clientHandler)) {
                int index = playerDuo.indexOf(clientHandler) == 1 ? 0 : 1;
                return playerDuo.get(index);
            }
        }
        return null;
    }

    public void attackHero(ClientHandler clientHandler, String selectedCard) {
        ClientHandler otherPlayer = getOtherPlayer(clientHandler);
        if (otherPlayer != null) {
            otherPlayer.send("attackHero");
            otherPlayer.send(selectedCard);
        }
    }

    public void damageHero(ClientHandler clientHandler, String damage) {
        ClientHandler otherPlayer = getOtherPlayer(clientHandler);
        if (otherPlayer != null) {
            otherPlayer.send("damageHero");
            otherPlayer.send(damage);
        }
    }

    public void notifyDisconnected(ClientHandler clientHandler) {
        ClientHandler otherPlayer = getOtherPlayer(clientHandler);
        if (otherPlayer != null) {
            otherPlayer.send("disconnect");
        }
    }

    public void notifyAbandon(ClientHandler clientHandler) {
        ClientHandler otherPlayer = getOtherPlayer(clientHandler);
        if (otherPlayer != null) {
            otherPlayer.send("abandon");
        }
    }

    public boolean registerPlayer(String player) {
        Account account = Utils.getAccountFromJson(player);
        String path = String.format("src/main/resources/players/%s.json", account.getUsername());
        if (new File(path).exists()) {
            return false;
        }
        try (FileWriter writer = new FileWriter(path)) {
            writer.write(player);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    public void removeClient(ClientHandler clientHandler) {
        clientHandler.interrupt();
        clients.remove(clientHandler);
    }

    public String loginPlayer(String username, String password) {
        Gson g = new Gson();
        try (BufferedReader br = new BufferedReader(new FileReader(String.format("src/main/resources/players/%s.json", username)))) {
            JsonObject object = g.fromJson(br, JsonObject.class);
            String player = object.toString();
            Account account = Utils.getAccountFromJson(player);
            if (!Password.decrypt(account.getPassword()).equals(password)) {
                return null;
            }
            br.close();
            return player;
        } catch (NullPointerException | IOException e) {
            return null;
        }
    }

    public void updatePlayerData(ClientHandler clientHandler, String playerData) throws IOException {
        String path = String.format("src/main/resources/players/%s.json", clientHandler.getAccount().getUsername());
        try (FileWriter writer = new FileWriter(path)) {
            writer.write(playerData);
        }
    }

    public void finishMatch(ClientHandler clientHandler) {
        playingPlayers.removeIf(playerDuo -> playerDuo.contains(clientHandler));
    }

    public void sendMessage(ClientHandler clientHandler, String chatMessage) {
        ClientHandler otherPlayer = getOtherPlayer(clientHandler);
        if (otherPlayer != null) {
            otherPlayer.send("chat");
            otherPlayer.send(chatMessage);
        }
    }

    public void getLeaderBoard() {
        List<String> names = new ArrayList<>();
        for (Account account : getAllClients()) {
            names.add(account.getUsername());
        }

        List<Integer> cups = new ArrayList<>();
        for (Account account : getAllClients()) {
            cups.add(account.getCups());
        }

        for (ClientHandler client : clients) {
            client.send("leaderBoard");
            client.send(getAsString(names));
            client.send(getAsString(cups));
        }
    }

    public List<Account> getAllClients() {
        List<Account> accounts = new ArrayList<>();
        Gson gson = new Gson();
        for (File f : Objects.requireNonNull(new File("./src/main/resources/players/").listFiles())) {
            try (BufferedReader br = new BufferedReader(new FileReader(f.getAbsolutePath()))) {
                accounts.add(gson.fromJson(br, Account.class));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return accounts;
    }


    public List<Card> getCards() {
        return cards;
    }

    public List<Hero.HeroClass> getHeroes() {
        return heroes;
    }

    public boolean isConnectedToDatabase() {
        return connectedToDatabase;
    }

    public List<String> getGameModes() {
        return gameModes;
    }
}
