import com.google.gson.Gson;

public class Utils {
    public static Account getAccountFromJson(String json){
        Gson gson = new Gson();
        return gson.fromJson(json, Account.class);
    }
}
