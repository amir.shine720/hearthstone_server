package card;

public class Card {
    private String name;
    private String description;
    private int manaCost;
    private int playCount;
    private Hero hero;
    private String type;
    private String rarity;
    private Ability ability;
    private int attack;

    private int health;

    public Card(String name, String description, int manaCost, Hero hero, String type, String rarity, int attack, int health) {
        this.name = name;
        this.description = description;
        this.manaCost = manaCost;
        this.hero = hero;
        this.rarity = rarity;
        this.type = type;
        this.attack = attack;
        this.health = health;
    }

    public Card() {
        this.manaCost = 0;
        this.hero = null;
        this.rarity = null;
    }


    public Ability getAbility() {
        return ability;
    }

    public int getManaCost() {
        return manaCost;
    }

    public void setManaCost(int manaConst) {
        this.manaCost = manaConst;
    }

    public int getPlayCount() {
        return playCount;
    }

    public Hero getHero() {
        return hero;
    }

    public String getRarity() {
        return rarity;
    }


    public String getType() {
        return type;
    }
}
