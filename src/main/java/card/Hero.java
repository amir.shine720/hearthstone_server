package card;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum Hero {
    MAGE("Mage", new HeroClass()),
    ROGUE("Rogue", new HeroClass()),
    WARLOCK("Warlock", new HeroClass()),
    HUNTER("Hunter", new HeroClass()),
    PALADIN("Paladin", new HeroClass()),
    PRIEST("Priest", new HeroClass()),
    NATURAL("Natural", new HeroClass());

    public static final Hero DEFAULT_HERO = MAGE;
    String name;
    HeroClass heroClass;

    Hero(String name, HeroClass hero) {
        this.name = name;
        this.heroClass = hero;
    }

    public static Hero getByName(String heroName) {
        for (Hero hero : getAllHeroes()) {
            if (hero.getName().toLowerCase().equals(heroName.toLowerCase())) {
                return hero;
            }
        }
        return null;
    }

    public static ArrayList<Hero> getAllHeroes() {
        return new ArrayList<>(Arrays.asList(Hero.values()));
    }

    public static void setHeroes(List<HeroClass> heroes) {
        for (HeroClass hero : heroes) {
            getByName(hero.getName()).heroClass = hero;
        }
    }

    public static List<HeroClass> getAllHeroClasses() {
        List<HeroClass> heroClasses = new ArrayList<>();
        for (Hero hero : getAllHeroes()) {
            heroClasses.add(hero.heroClass);
        }
        return heroClasses;
    }

    public String getName() {
        return name;
    }

    public ArrayList<String> getDefaultCards() {
        if(heroClass.defaultCards == null){
            heroClass.defaultCards = new ArrayList<>();
        }
        return heroClass.defaultCards;
    }

    public String getSpecialPower() {
        return heroClass.specialPower;
    }

    public HeroPower getHeroPower() {
        return heroClass.heroPower;
    }


    public static class HeroClass {
        private String name;
        private ArrayList<String> defaultCards;
        String specialPower;
        HeroPower heroPower;

        public HeroClass() {
        }

        public HeroClass(String name, ArrayList<String> defaultCards, String specialPower, HeroPower heroPower) {
            this.name = name;
            this.defaultCards = defaultCards;
            this.specialPower = specialPower;
            this.heroPower = heroPower;
        }

        public String getName() {
            return name;
        }

        public void addDefaultCard(String defaultCard) {
            if(defaultCards == null){
                defaultCards = new ArrayList<>();
            }
            defaultCards.add(defaultCard);
        }
    }

}
