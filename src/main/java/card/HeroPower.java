package card;

public class HeroPower {
    private String name;
    private String description;
    private int manaCost;
    private boolean needTarget;

    public HeroPower(String name, String description, int manaCost, boolean needTarget) {
        this.name = name;
        this.description = description;
        this.manaCost = manaCost;
        this.needTarget = needTarget;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getManaCost() {
        return manaCost;
    }

    public void setManaCost(int manaCost) {
        this.manaCost = manaCost;
    }

    public boolean isNeedTarget() {
        return needTarget;
    }

    public void setNeedTarget(boolean needTarget) {
        this.needTarget = needTarget;
    }
}
