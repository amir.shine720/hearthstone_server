package card;

public enum Type {
    MINION("Minion"),
    SPELL("Spell"),
    WEAPON("Weapon"),
    PASSIVE("Passive"),
    QUEST("Quest");

    String name;

    Type(String name) {
        this.name = name;
    }
}
