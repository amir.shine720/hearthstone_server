package database;

public class MySqlMain {
    MySqlWriter writer;
    MySqlReader reader;

    public MySqlMain(){
        writer = new MySqlWriter();
        reader = new MySqlReader();
    }

    public MySqlWriter getWriter() {
        return writer;
    }

    public MySqlReader getReader() {
        return reader;
    }
}
