package database;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MySqlReader {


    public List<Map<String, Object>> getDataSet(String query, List<String> neededKeys) throws SQLException {
        try (Connection connection = DriverManager
                .getConnection("jdbc:mysql://localhost:3306/hearthstone?useSSL=false", "root", "root");
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(query)) {

            List<Map<String, Object>> mapList = new ArrayList<>();

            while (resultSet.next()) {
                Map<String, Object> resultMap = new HashMap<>();
                for (String key : neededKeys) {
                    resultMap.put(key, resultSet.getObject(key));
                }
                mapList.add(resultMap);
            }

            return mapList;
        }
    }
}
