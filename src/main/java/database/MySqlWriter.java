package database;

import java.sql.*;

public class MySqlWriter {
    private static final String createTableSQL = "create table users";
    private static final String insertDataSQL = "INSERT INTO users" + " (id, username, password) VALUES " + " (?,?,?);";
    private static final String selectDataSQL = "select id,username,password from users where id = ?";
    private static final String deleteDataSQL = "delete from users where id = ?;";

    public void createTable() throws SQLException {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://");
             Statement statement = connection.createStatement();) {

            statement.execute(createTableSQL);
        }
    }

    public void insertData() throws SQLException {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://");
             PreparedStatement preparedStatement = connection.prepareStatement(insertDataSQL)
        ) {
            preparedStatement.setInt(1, 1);
            preparedStatement.setString(2, "xerxes");
            preparedStatement.setString(3, "paliz123");

            preparedStatement.executeUpdate();
        }
    }

    public void updateData() throws SQLException{
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://");
             PreparedStatement preparedStatement = connection.prepareStatement(selectDataSQL)
        ) {
            preparedStatement.setInt(1, 1);

            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.executeQuery();
        }
    }

    public void deleteData() throws SQLException{
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://");
             PreparedStatement preparedStatement = connection.prepareStatement(deleteDataSQL)
        ) {
            preparedStatement.setInt(1, 1);

            preparedStatement.executeUpdate();
        }
    }

}
